# My Custom UI Components - Documentation
Selamat datang di dokumentasi "My Custom UI Components"! Library ini berisi kumpulan komponen UI kustom yang dirancang untuk membantu Anda dalam mengembangkan aplikasi React dengan antarmuka yang menarik dan fungsional. Berikut adalah daftar komponen yang tersedia beserta penjelasan dan cara penggunaannya.

## Instalasi
Anda dapat menginstal "My Custom UI Components" menggunakan npm:

```
npm install my-custom-ui-components
```
atau menggunakan yarn:
```
yarn add my-custom-ui-components
```
Penggunaan
Untuk menggunakan komponen-komponen dari library ini, Anda perlu mengimpor komponen yang diinginkan ke dalam proyek React Anda.

Contoh penggunaan:

```
import React from 'react';
import { Button, Modal, Card } from 'my-custom-ui-components';

const App = () => {
  return (
    <div>
      <Button label="Click Me" onClick={() => console.log('Button clicked!')} />
      <Modal isOpen={true}>
        <h2>Hello, this is a modal!</h2>
        <p>Modal content goes here...</p>
      </Modal>
      <Card title="Example Card">
        <p>This is the content of the card.</p>
      </Card>
    </div>
  );
};

export default App;
```

## Komponen yang Tersedia
### Button
Komponen tombol yang dapat digunakan untuk interaksi pengguna.

Properti:

- label (string, wajib) - Label teks pada tombol.
- onClick (function, wajib) - Fungsi yang akan dijalankan saat tombol diklik.

### Modal
Komponen jendela modal untuk menampilkan konten tambahan di atas halaman utama.

Properti:

- isOpen (boolean, wajib) - Menentukan apakah modal ditampilkan atau disembunyikan.
- onClose (function, opsional) - Fungsi yang akan dijalankan saat modal ditutup.

### Card
Komponen kartu dengan judul dan konten yang dapat digunakan untuk menampilkan konten terorganisir.

Properti:

- title (string, wajib) - Judul kartu.
- children (React node, wajib) - Konten kartu dalam bentuk elemen React.

## Kontribusi
Kami menghargai kontribusi dari para pengguna "My Custom UI Components" untuk membuat library ini lebih baik. Jika Anda menemukan masalah, ingin menambahkan fitur baru, atau memberikan perbaikan, silakan buat Pull Request di repositori GitHub kami.

## Lisensi
Library "My Custom UI Components" dilisensikan di bawah MIT License.

## Kontak
Jika Anda memiliki pertanyaan atau masukan, jangan ragu untuk menghubungi kami melalui email di rhesadav48@gmail.com.