import React, { ButtonHTMLAttributes } from 'react';
import styled, { keyframes } from 'styled-components';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  primary?: boolean;
}

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const StyledButton = styled.button<ButtonProps>`
  padding: 10px 20px;
  font-size: 16px;
  background-color: ${props => (props.primary ? '#3498db' : '#ffffff')};
  color: ${props => (props.primary ? '#ffffff' : '#3498db')};
  border: 2px solid #3498db;
  border-radius: 5px;
  cursor: pointer;
  transition: background-color 0.3s ease;
  animation: ${fadeIn} 0.5s ease; // Menambahkan animasi keyframes saat tombol muncul

  &:hover {
    background-color: ${props => (props.primary ? '#2980b9' : '#f2f2f2')};
    transform: scale(1.05); // Efek scaling saat tombol di-hover
  }

  &:focus {
    outline: none;
  }
`;

const Button: React.FC<ButtonProps> = ({ primary, children, ...restProps }) => {
  return (
    <StyledButton primary={primary} {...restProps}>
      {children}
    </StyledButton>
  );
};

export default Button;
