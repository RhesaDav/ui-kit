import React, { useRef } from 'react';
import styled, { keyframes } from 'styled-components';

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  children: React.ReactNode
}

// Membuat animasi keyframes
const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

interface ModalWrapperProps {
  isOpen: boolean;
}

const ModalWrapper = styled.div<ModalWrapperProps>`
  display: ${props => (props.isOpen ? 'block' : 'none')};
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 9999;
  animation: ${fadeIn} 0.3s ease; // Menambahkan animasi keyframes saat modal muncul
`;

const ModalContent = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #ffffff;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.3);
  min-width: 300px;
  max-width: 80%;
`;

const CloseButton = styled.button`
  position: absolute;
  top: 10px;
  right: 10px;
  font-size: 16px;
  background-color: transparent;
  border: none;
  cursor: pointer;

  &:hover {
    color: #e74c3c; // Efek warna saat tombol ditahan
  }

  &:focus {
    outline: none;
  }
`;

const Modal: React.FC<ModalProps> = ({ isOpen, onClose, children }) => {
  const modalRef = useRef<HTMLDivElement>(null);

  // useEffect(() => {
  //   function handleEscape(event: KeyboardEvent) {
  //     if (event.key === 'Escape') {
  //       onClose();
  //     }
  //   }

  //   function handleClickOutside(event: MouseEvent) {
  //     if (modalRef.current && !modalRef.current.contains(event.target as Node)) {
  //       onClose();
  //     }
  //   }

  //   if (isOpen) {
  //     document.addEventListener('keydown', handleEscape);
  //     document.addEventListener('click', handleClickOutside);
  //   }

  //   return () => {
  //     document.removeEventListener('keydown', handleEscape);
  //     document.removeEventListener('click', handleClickOutside);
  //   };
  // }, [isOpen, onClose]);

  return (
    <ModalWrapper isOpen={isOpen}>
      <ModalContent ref={modalRef}>
        <CloseButton onClick={onClose}>X</CloseButton>
        {children}
      </ModalContent>
    </ModalWrapper>
  );
};

export default Modal;
