import React from 'react';
import styled, { keyframes } from 'styled-components';

interface CardProps {
  title: string;
  children: React.ReactNode;
}

// Membuat animasi keyframes
const fadeIn = keyframes`
  from {
    opacity: 0;
    transform: translateY(-10px);
  }
  to {
    opacity: 1;
    transform: translateY(0);
  }
`;

const CardWrapper = styled.div`
  border: 1px solid #ccc;
  border-radius: 5px;
  padding: 20px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  animation: ${fadeIn} 0.5s ease; // Menambahkan animasi keyframes saat kartu muncul

  &:hover {
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2); // Efek shadow saat kartu di-hover
    transform: scale(1.02); // Efek scaling saat kartu di-hover
  }
`;

const Title = styled.h2`
  font-size: 18px;
  margin-bottom: 10px;
`;

const Card: React.FC<CardProps> = ({ title, children }) => {
  return (
    <CardWrapper>
      <Title>{title}</Title>
      {children}
    </CardWrapper>
  );
};

export default Card;
