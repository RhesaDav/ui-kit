import React, { InputHTMLAttributes } from 'react';
import styled, { keyframes } from 'styled-components';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  // Tambahkan properti khusus yang Anda butuhkan untuk input
}

// Membuat animasi keyframes
const fadeIn = keyframes`
  from {
    opacity: 0;
    transform: translateY(-5px);
  }
  to {
    opacity: 1;
    transform: translateY(0);
  }
`;

const InputWrapper = styled.input<InputProps>`
  padding: 10px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 5px;
  transition: border-color 0.3s ease;
  animation: ${fadeIn} 0.5s ease; // Menambahkan animasi keyframes saat input muncul

  &:hover {
    border-color: #3498db; // Efek border saat input di-hover
  }

  &:focus {
    outline: none;
    border-color: #3498db; // Efek border saat input mendapatkan fokus
    box-shadow: 0 0 5px #3498db; // Efek shadow saat input mendapatkan fokus
  }
`;

const Input: React.FC<InputProps> = ({ ...restProps }) => {
  return <InputWrapper {...restProps} />;
};

export default Input;
