import { useState } from "react";
import { Button, Card, Input, Modal } from "./components";

function App() {
  const [showModal, setShowModal] = useState(false);

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  console.log(showModal)
  return (
    <>
      <div style={{ display: "flex", gap: 5 }}>
        <Card title="Kartu 1">
          <p>Ini adalah konten dari kartu 1.</p>
        </Card>
        <Card title="Kartu 2">
          <p>Ini adalah konten dari kartu 2.</p>
        </Card>
      </div>
      <Input />
      <Button onClick={handleModalOpen}>Open Modal</Button>
      <Modal isOpen={showModal} onClose={handleModalClose}>
        <h2>Ini adalah judul modal</h2>
        <p>Ini adalah konten dari modal.</p>
      </Modal>
    </>
  );
}

export default App;
